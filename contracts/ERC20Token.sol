// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/presets/ERC20PresetMinterPauser.sol";

contract ERC20Token is ERC20PresetMinterPauser {
    uint8 _decimal = 18;

    constructor(string memory name_, string memory symbol_, uint8 decimal_) ERC20PresetMinterPauser(name_, symbol_) {
        _decimal = decimal_;
    }

    function decimals() public view virtual override returns (uint8) {
        return _decimal;
    }
}