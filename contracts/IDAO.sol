// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

interface IDAO {
    function allVotesFinished(address voterAddress) external returns(bool);
    function setStakingAddress(address newStaking) external;
}