// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

contract Authorization {

    //referral => referrer
    mapping (address => address) _referral;

    modifier onlyRegistered() {
        require(_referral[msg.sender] != address(0), "Authorization: need registration");
        _;
    }

    modifier onlyRegisteredReferrer(address accountAddress) {
        require(isRegistered(accountAddress), "Authorization:referrer not known");
        _;
    }

    function isRegistered(address accountAddress) public view returns(bool) {
        return _referral[accountAddress] != address(0);
    }

    function getReferrer(address referralAddress) external view returns(address referrer){
        return _referral[referralAddress];
    }

    function signUpWithoutReferrer() external {
        address sender = msg.sender;
        _register(sender, address(0));
    }

    function signUp(address referrer) external onlyRegisteredReferrer(referrer) {
        address sender = msg.sender;
        _register(sender, referrer);
    }

    function setReferrer(address newReferrer) external onlyRegistered onlyRegisteredReferrer(newReferrer) {
        address sender = msg.sender;
        address referre = _referral[sender];
        require(referre == address(this), "Authorization: has referrer");

        _referral[sender] = newReferrer;
    }

    function _register(address sender, address referrer) internal {
        require(_referral[sender] == address(0), "Authorization:already authorized");

        if(referrer == address(0)) {
            _referral[sender] = address(this);
        } else {
            _referral[sender] = referrer;
        }
    }
}