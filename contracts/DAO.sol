// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "./IStaking.sol";
import "./IDAO.sol";

contract DAO is IDAO {

    uint64 public minimumQuorum;
    uint64 public debatingPeriodDuration;
    address public chairPerson;
    address public stakingAddress;

    uint256 public proposalsLength;
    //proposal id => Proposal 
    //id start from 1
    mapping(uint256 => Proposal) _proposals;
    mapping(address => Voter) _voters;

    struct Proposal {
        uint64 endDate;
        bool finished;
        uint128 countVoteDisagree;
        uint128 countVoteAgree;
        address recipient;
        bytes callData;
        //voter address => isVoted
        mapping(address => bool) votedProposals;
    }

    struct Voter {
        uint256 lengthVotedProposal;
        // voteId => proposalId
        mapping(uint256 => uint256) votedProposalIds;
    }

    constructor(address chairPerson_, uint64 minimumQuorum_, uint64 debatingPeriodDuration_) {
        chairPerson = chairPerson_;
        minimumQuorum = minimumQuorum_;
        debatingPeriodDuration = debatingPeriodDuration_;
    }

    modifier onlyChairPerson() {
        require(msg.sender == chairPerson, "DAO: Only chair person");
        _;
    }

    modifier proposalMustBeCreated(uint256 proposalId) {
        require(proposalId > 0 && proposalId <= proposalsLength, "DAO:proposal hasn't been created");
        _;
    }

    function proposal(uint256 proposalId) public view proposalMustBeCreated(proposalId) returns(
        uint64 endDate,
        uint128 countVoteAgree,
        uint128 countVoteDisagree,
        bool finished,
        address recipient,
        bytes memory callData
    ) {
        return (
        _proposals[proposalId].endDate,
        _proposals[proposalId].countVoteAgree,
        _proposals[proposalId].countVoteDisagree,
        _proposals[proposalId].finished,
        _proposals[proposalId].recipient,
        _proposals[proposalId].callData
       );
    }

    function lengthVotedProposal(address voterAddress) public view returns(uint256 lengthVotedProposal) {
        return  _voters[voterAddress].lengthVotedProposal;
    }

    function setStakingAddress(address newStaking) external virtual override onlyChairPerson {
        stakingAddress = newStaking;
    }

    function allVotesFinished(address voterAddress) public virtual override returns(bool) {

        Voter storage currentVoter =  _voters[voterAddress];

        for(uint256 voteId = 0; voteId < currentVoter.lengthVotedProposal; voteId++) {
            uint256 proposalId = _voters[voterAddress].votedProposalIds[voteId];
            if(!_proposals[proposalId].finished) {
                return false;
            }
        }

        currentVoter.lengthVotedProposal = 0;
        return true;
    }

    function addProposal(bytes memory callData, address recipient) external onlyChairPerson() {
        proposalsLength++;
        uint256 curentVoteId = proposalsLength;
        uint64 currentTime = uint64(block.timestamp);
        Proposal storage currentProposal = _proposals[curentVoteId];
        currentProposal.callData = callData;
        currentProposal.recipient = recipient;
        currentProposal.endDate = currentTime + debatingPeriodDuration;
    }

    function vote(uint256 proposalId, bool agree) external proposalMustBeCreated(proposalId) {
        address sender = msg.sender;

        Proposal storage currentProposal = _proposals[proposalId];

        require(!currentProposal.votedProposals[sender], "DAO: has voted for this proposal");

        uint64 currentTime = uint64(block.timestamp);
        require(currentTime < currentProposal.endDate, "DAO: time voting has ended");

        uint128 deposit = IStaking(stakingAddress).getLockedTokenAmount(sender);
        require(deposit > 0, "DAO:stake must be greater than 0");

        if(agree) {
            currentProposal.countVoteAgree += deposit;
        } else {
            currentProposal.countVoteDisagree += deposit;
        }

        currentProposal.votedProposals[sender] = true;

        Voter storage currentVoter = _voters[sender];

        currentVoter.votedProposalIds[currentVoter.lengthVotedProposal] = proposalId;
        currentVoter.lengthVotedProposal++;
    }

    function finishProposal(uint128 proposalId) external proposalMustBeCreated(proposalId) {
        Proposal storage currentProposal = _proposals[proposalId];
        require(!currentProposal.finished, "DAO: already finished");
        uint64 currentTime = uint64(block.timestamp);
        require(currentTime >= currentProposal.endDate, "DAO: time voting has not ended");

        currentProposal.finished = true;
        if(
            ((currentProposal.countVoteAgree + currentProposal.countVoteDisagree) >= minimumQuorum) && 
            currentProposal.countVoteAgree > currentProposal.countVoteDisagree
        ) {
            (bool success, ) = currentProposal.recipient.call{value:0 } (currentProposal.callData);
            require(success, "ERROR call function");
        }
    }
}