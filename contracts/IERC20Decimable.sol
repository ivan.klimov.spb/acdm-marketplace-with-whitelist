// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

interface IERC20Decimable {
    function decimals() external view returns (uint8);
}