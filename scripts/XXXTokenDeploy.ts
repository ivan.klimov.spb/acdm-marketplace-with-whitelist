import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { ERC20Token } from "../src/types";

async function main() {
  const factory = await ethers.getContractFactory("ERC20Token");
  console.log("Deploying XXXToken...");

  const contract: ERC20Token = await factory.deploy("XXX Coin", "XXX", 18);

  await contract.deployed();

  console.log("XXXToken deployed to:", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
