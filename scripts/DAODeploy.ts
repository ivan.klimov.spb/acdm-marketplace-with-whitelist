import dotenv from "dotenv";
dotenv.config({ path: __dirname + "/.env" });

import { ethers } from "hardhat";
import { DAO } from "../src/types";

const { PUBLIC_KEY } = process.env;

async function main() {
  const factory = await ethers.getContractFactory("DAO");
  console.log("Deploying DAO...");

  const contract: DAO = await factory.deploy(
    PUBLIC_KEY as string,
    10000000,
    60 * 60 * 24 * 3 // 3 days
  );

  await contract.deployed();

  console.log("DAO deployed to: ", contract.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
