const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";
import { deploymentXXXToken, MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import { getTimestamp } from "../getTimestamp";
import { addLiquidityETH } from "./addLiquidity";
import { getMerkleTree, getProof } from "./MarkleTree";

export const defaultStakeAmount = 10000;
export async function deployStaking(lpToken: IERC20, XXXToken: ERC20Token) {
  let accounts: SignerWithAddress[];
  let Staking: Staking;

  accounts = await ethers.getSigners();

  const factoryStaking = await ethers.getContractFactory("Staking");
  Staking = await factoryStaking.deploy(lpToken.address, XXXToken.address);
  await Staking.deployed();

  //XXXToken.grantRole(await XXXToken.MINTER_ROLE(), Staking.address);

  return Staking;
}

export async function initStakingForTests(
  XXXToken: ERC20Token,
  lpToken: IERC20,
  accounts: SignerWithAddress[]
) {
  await XXXToken.mint(accounts[1].address, MINT_XXX_TOKEN.mul(2));
  await XXXToken.mint(accounts[2].address, MINT_XXX_TOKEN.mul(2));
  await XXXToken.mint(accounts[3].address, MINT_XXX_TOKEN.mul(2));

  lpToken = await addLiquidityETH(XXXToken, 1);
  await addLiquidityETH(XXXToken, 2);
  await addLiquidityETH(XXXToken, 3);

  let stakingContract = await deployStaking(lpToken, XXXToken);

  let merkleTree = await getMerkleTree();
  let rootMerkle = merkleTree.getHexRoot();

  stakingContract.setDAO(accounts[1].address);

  await stakingContract.connect(accounts[1]).setMerkleRoot(rootMerkle);

  await lpToken
    .connect(accounts[1])
    .approve(stakingContract.address, ethers.constants.MaxUint256);

  await lpToken
    .connect(accounts[2])
    .approve(stakingContract.address, ethers.constants.MaxUint256);

  await lpToken
    .connect(accounts[3])
    .approve(stakingContract.address, ethers.constants.MaxUint256);

  await stakingContract
    .connect(accounts[1])
    .stake(defaultStakeAmount, await getProof(1, merkleTree));
  await stakingContract
    .connect(accounts[2])
    .stake(defaultStakeAmount, await getProof(2, merkleTree));
  await stakingContract
    .connect(accounts[3])
    .stake(defaultStakeAmount, await getProof(3, merkleTree));

  await XXXToken.grantRole(
    await XXXToken.MINTER_ROLE(),
    stakingContract.address
  );

  return stakingContract;
}
