const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ERC20Token,
  IUniswapV2Router01,
  IUniswapV2Factory,
} from "../../src/types";
import { BigNumber } from "ethers";
import { MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import { getTimestamp } from "../getTimestamp";

export async function addLiquidityETH(XXXToken: ERC20Token, accountId = 0) {
  let uniswapRoter: IUniswapV2Router01;
  let uniswapFactory: IUniswapV2Factory;
  let accounts: SignerWithAddress[] = await ethers.getSigners();

  await XXXToken.mint(accounts[accountId].address, MINT_XXX_TOKEN);

  uniswapFactory = <IUniswapV2Factory>(
    await ethers.getContractAt(
      "IUniswapV2Factory",
      process.env.FACTORY_ADDRESS as string
    )
  );

  uniswapRoter = <IUniswapV2Router01>(
    await ethers.getContractAt(
      "IUniswapV2Router01",
      process.env.ROUTER_ADDRESS as string
    )
  );

  await XXXToken.connect(accounts[accountId]).approve(
    uniswapRoter.address,
    ethers.constants.MaxUint256
  );

  const timestemp: number = await getTimestamp();

  let result = await uniswapRoter
    .connect(accounts[accountId])
    .addLiquidityETH(
      XXXToken.address,
      MINT_XXX_TOKEN,
      MINT_XXX_TOKEN,
      ethers.utils.parseEther("1"),
      accounts[accountId].address,
      timestemp + 30,
      {
        value: ethers.utils.parseEther("1"), //eth value
      }
    );

  const wethTokenAddr = await uniswapRoter.WETH();
  let addressLP = await uniswapFactory.getPair(XXXToken.address, wethTokenAddr);

  let lpToken = await ethers.getContractAt("IERC20", addressLP);

  return lpToken;
}
