const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { defaultStakeAmount, deployStaking } from "./deploymentStaking";
import { getTimestamp } from "../getTimestamp";
import MerkleTree from "merkletreejs";
import { getMerkleTree, getProof } from "./MarkleTree";

describe("Claim Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;
  let rootMerkle: string;
  let merkleTree: MerkleTree;
  let proof: string[];

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    stakingContract = await deployStaking(lpToken, XXXToken);

    await XXXToken.grantRole(
      await XXXToken.MINTER_ROLE(),
      stakingContract.address
    );

    lpToken
      .connect(accounts[0])
      .approve(stakingContract.address, ethers.constants.MaxUint256);

    lpToken
      .connect(accounts[1])
      .approve(stakingContract.address, ethers.constants.MaxUint256);

    stakingContract.setDAO(accounts[1].address);

    merkleTree = await getMerkleTree();
    rootMerkle = merkleTree.getHexRoot();

    await stakingContract.connect(accounts[1]).setMerkleRoot(rootMerkle);

    proof = await getProof(1, merkleTree);

    await stakingContract.connect(accounts[1]).stake(defaultStakeAmount, proof);

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await stakingContract.rewardCircleTimer()) + 1),
    ]);
  });

  it("Should claim the right reward", async function () {
    let balanceBeforeClaim = (
      await XXXToken.balanceOf(accounts[1].address)
    ).toNumber();

    await stakingContract.connect(accounts[1]).claim();

    let balanceAfterClaim = (
      await XXXToken.balanceOf(accounts[1].address)
    ).toNumber();

    expect(balanceAfterClaim - balanceBeforeClaim).to.equal(300);
  });

  describe("Reverted", function () {
    it("try claim with amount 0 in stake: Not amount in stake", async () => {
      await expect(stakingContract.claim()).to.be.revertedWith(
        "Staking: User has not staked yet"
      );
    });
  });

  describe("Emit", function () {
    it("Emit Claimed(address[1], 300)", async () => {
      await expect(stakingContract.connect(accounts[1]).claim())
        .to.emit(stakingContract, "Claimed")
        .withArgs(accounts[1].address, 300);
    });
  });
});
