const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ERC20Token, IERC20, Staking } from "../../src/types";
import { BigNumber } from "ethers";

import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { addLiquidityETH } from "./addLiquidity";
import { deployStaking } from "./deploymentStaking";
import { getMerkleTree } from "./MarkleTree";

describe("SetLockTime Staking", function () {
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let accounts: SignerWithAddress[];
  let stakingContract: Staking;
  let rootMerkle: string;
  let merkleTree;

  beforeEach(async function () {
    accounts = await ethers.getSigners();

    XXXToken = await deploymentXXXToken();

    lpToken = await addLiquidityETH(XXXToken);
    await addLiquidityETH(XXXToken, 1);

    stakingContract = await deployStaking(lpToken, XXXToken);

    stakingContract.setDAO(accounts[1].address);

    merkleTree = await getMerkleTree();
    rootMerkle = merkleTree.getHexRoot();
  });

  it("Should be set right locktime", async function () {
    await stakingContract.connect(accounts[1]).setMerkleRoot(rootMerkle);

    expect(await stakingContract.merkleRoot()).to.equal(rootMerkle);
  });
});
