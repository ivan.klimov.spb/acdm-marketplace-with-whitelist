import { MerkleTree } from "merkletreejs";

import dotenv from "dotenv";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { ethers } from "hardhat";

export async function getMerkleTree() {
  let accounts: SignerWithAddress[] = await ethers.getSigners();

  let whitelistAddresses = [
    accounts[0].address,
    accounts[1].address,
    accounts[2].address,
    accounts[3].address,
    accounts[4].address,
    accounts[5].address,
    accounts[6].address,
  ];

  const leafNodes = whitelistAddresses.map((addr) =>
    ethers.utils.keccak256(addr)
  );

  const merkleTree = new MerkleTree(leafNodes, ethers.utils.keccak256, {
    sortPairs: true,
  });

  return merkleTree;
}

export async function getProof(accountId: number, merkleTree: MerkleTree) {
  let accounts: SignerWithAddress[] = await ethers.getSigners();
  const leaf = ethers.utils.keccak256(accounts[accountId].address);
  const proof = merkleTree.getHexProof(leaf);
  return proof;
}
