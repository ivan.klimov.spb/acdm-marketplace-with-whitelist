const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import { DAO } from "../../src/types";
import { defaultStakeAmount } from "../Staking/deploymentStaking";

export const chairPersonId = 5;
export const minimumQuorum = defaultStakeAmount;
export const debatingPeriodDuration = 100;
export async function deployDAO() {
  let accounts: SignerWithAddress[] = await ethers.getSigners();
  let chairPerson = accounts[chairPersonId];

  const factoryStaking = await ethers.getContractFactory("DAO");
  let dao: DAO = await factoryStaking.deploy(
    chairPerson.address,
    minimumQuorum,
    debatingPeriodDuration
  );
  await dao.deployed();

  return dao;
}
