const { expect } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  DAO,
  ERC20Token,
  ERC20Token__factory,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "./deploymentDAO";
import { deploymentXXXToken, MINT_XXX_TOKEN } from "../XXXToken/deployXXXToken";
import { getTimestamp } from "../getTimestamp";
import { addLiquidityETH } from "../Staking/addLiquidity";
import {
  defaultStakeAmount,
  deployStaking,
  initStakingForTests,
} from "../Staking/deploymentStaking";

describe("Vote ", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken(); //not important which erc20

    dao = await deployDAO();

    calldata = ERC20Token__factory.createInterface().encodeFunctionData(
      "mint",
      [accounts[1].address, MINT_XXX_TOKEN]
    );

    recipient = XXXToken.address;

    stakingContract = await initStakingForTests(XXXToken, lpToken, accounts);

    await dao.connect(chairPerson).addProposal(calldata, recipient);
    await dao.connect(chairPerson).setStakingAddress(stakingContract.address);

    await XXXToken.grantRole(await XXXToken.MINTER_ROLE(), dao.address); // fo testing
  });

  it("Should voted agree eq 0 before vote", async function () {
    const agree = true;

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteAgree).to.equal(0);
  });

  it("Should voted disagree eq 0 before vote", async function () {
    const agree = true;

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteDisagree).to.equal(0);
  });

  it("Should voted agree and correct increase", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteAgree).to.equal(defaultStakeAmount);
  });

  it("Should voted disagree and correct increase", async function () {
    const disagree = false;

    await dao.connect(accounts[1]).vote(1, disagree);

    let proposal = await dao.proposal(1);

    expect(proposal.countVoteDisagree).to.equal(defaultStakeAmount);
  });

  it("Should after one vote lengthVotedProposal = 1", async function () {
    const disagree = false;

    await dao.connect(accounts[1]).vote(1, disagree);

    expect(await dao.lengthVotedProposal(accounts[1].address)).to.equal(1);
  });

  it("After first vote will add increase deposit and vote for second proposal, but voting for first wil not changed from this account", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);

    let proposal = await dao.proposal(1);
    const proposaleVoteAgreeBeforeDeposit = proposal.countVoteAgree.toNumber();

    await dao.connect(chairPerson).addProposal(calldata, recipient);

    await dao.connect(accounts[1]).vote(2, agree);

    const proposaleVoteAfterBeforeDeposit = proposal.countVoteAgree.toNumber();

    expect(
      proposaleVoteAfterBeforeDeposit - proposaleVoteAgreeBeforeDeposit
    ).to.equal(0);
  });

  it("After first vote will add increase deposit and vote for second proposal will be increase", async function () {
    const agree = true;

    await dao.connect(accounts[1]).vote(1, agree);
    await dao.connect(chairPerson).addProposal(calldata, recipient);

    await dao.connect(accounts[1]).vote(2, agree);
    let proposal = await dao.proposal(2);
    const secondProposalResoltVote = await proposal.countVoteAgree.toNumber();
  });

  describe("Reverted", function () {
    it("has voted for this proposal", async () => {
      const agree = true;

      await dao.connect(accounts[1]).vote(1, agree);

      await expect(dao.connect(accounts[1]).vote(1, agree)).to.be.revertedWith(
        "DAO: has voted for this proposal"
      );
    });

    it("time voting has ended", async () => {
      const agree = true;

      const timeStamp = await getTimestamp();
      await ethers.provider.send("evm_mine", [
        timeStamp + (await dao.debatingPeriodDuration()).toNumber(),
      ]);

      await expect(dao.connect(accounts[1]).vote(1, agree)).to.be.revertedWith(
        "DAO: time voting has ended"
      );
    });

    it("deposit must be greater than zero", async () => {
      const agree = true;
      await expect(dao.connect(accounts[4]).vote(1, agree)).to.be.revertedWith(
        "DAO:stake must be greater than 0"
      );
    });
  });
});
