const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

describe("authorization ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    /*await XXXToken.mint(accounts[1].address, MINT_XXX_TOKEN.mul(2));
    lpToken = await addLiquidityETH(XXXToken, 1);

    stakingContract = await initStakingForTests(XXXToken, lpToken, accounts);*/

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);
  });

  it("Should not registered by default", async function () {
    expect(await ACDMPlatform.isRegistered(accounts[1].address)).to.be.false;
  });

  it("Should signUpWithoutReferrer the account with platform referrer", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();

    expect(await ACDMPlatform.isRegistered(accounts[1].address)).to.be.true;
  });

  it("Should signUpWithoutReferrer the account with platform referrer", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();

    expect(await ACDMPlatform.getReferrer(accounts[1].address)).to.equal(
      ACDMPlatform.address
    );
  });

  it("Should signup with param accounts[2] the account with accounts[2] referrer", async function () {
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[1]).signUp(accounts[2].address);

    expect(await ACDMPlatform.getReferrer(accounts[1].address)).to.equal(
      accounts[2].address
    );
  });

  it("Should set referrer for account if his referrer eq ACDMPlatform", async function () {
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();

    await ACDMPlatform.connect(accounts[1]).setReferrer(accounts[2].address);

    expect(await ACDMPlatform.getReferrer(accounts[1].address)).to.equal(
      accounts[2].address
    );
  });

  describe("Reverted", function () {
    it("Authorization:already authorized", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();

      await expect(
        ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer()
      ).to.be.revertedWith("Authorization:already authorized");
    });

    it("Authorization: has referrer", async () => {
      await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
      await ACDMPlatform.connect(accounts[3]).signUpWithoutReferrer();
      await ACDMPlatform.connect(accounts[1]).signUp(accounts[2].address);

      await expect(
        ACDMPlatform.connect(accounts[1]).setReferrer(accounts[3].address)
      ).to.be.revertedWith("Authorization: has referrer");
    });

    it("Authorization: need registration", async () => {
      await expect(
        ACDMPlatform.connect(accounts[1]).setReferrer(accounts[3].address)
      ).to.be.revertedWith("Authorization: need registration");
    });

    it("Authorization:referrer not known", async () => {
      await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
      await expect(
        ACDMPlatform.connect(accounts[1]).setReferrer(accounts[3].address)
      ).to.be.revertedWith("Authorization:referrer not known");
    });
  });
});
