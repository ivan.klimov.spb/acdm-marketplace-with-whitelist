const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

const purchasedOnSale: BigNumber = BigNumber.from(50000000000);

describe("trade buy ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);

    ACDMToken.connect(accounts[1]).approve(
      ACDMPlatform.address,
      ethers.constants.MaxUint256
    );

    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.5"),
    });

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);

    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).initSell(
      purchasedOnSale,
      await ACDMPlatform.ACDMTokenPrice()
    );
  });

  it("Should transfer tokens from ACDMPlatform", async function () {
    const ACDMPlatformBalanceTokenBefore = await ACDMToken.balanceOf(
      ACDMPlatform.address
    );

    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
    const value = tradeSell.price.mul(tradeSell.amount).div(2);

    await ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
      value: value,
    });

    const ACDMPlatformBalanceTokenAfter = await ACDMToken.balanceOf(
      ACDMPlatform.address
    );

    expect(
      ACDMPlatformBalanceTokenAfter.sub(ACDMPlatformBalanceTokenBefore)
    ).to.eq(tradeSell.amount.div(2).mul(-1));
  });

  it("Should transfer tokens to customer", async function () {
    const ACDMPlatformBalanceTokenBefore = await ACDMToken.balanceOf(
      accounts[2].address
    );

    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
    const value = tradeSell.price.mul(tradeSell.amount).div(2);

    await ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
      value: value,
    });

    const ACDMPlatformBalanceTokenAfter = await ACDMToken.balanceOf(
      accounts[2].address
    );

    expect(
      ACDMPlatformBalanceTokenAfter.sub(ACDMPlatformBalanceTokenBefore)
    ).to.eq(tradeSell.amount.div(2));
  });

  it("Should correct amount remainder for seller", async function () {
    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
    const value = tradeSell.price.mul(tradeSell.amount).div(2);

    await ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
      value: value,
    });

    let remainder = (await ACDMPlatform.getTradeSale(accounts[1].address))
      .amount;

    expect(remainder).to.eq(tradeSell.amount.div(2));
  });

  it("Should send correct eth to seller", async function () {
    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
    const value = tradeSell.price.mul(tradeSell.amount).div(2);

    let percentageReferrerTrading =
      await ACDMPlatform.percentageReferrerTrading();
    let hundredPercent = await ACDMPlatform.hundredPercent();

    await expect(() =>
      ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
        value: value,
      })
    ).to.changeEtherBalances(
      [accounts[1]],
      [
        value
          .mul(
            hundredPercent -
              percentageReferrerTrading -
              percentageReferrerTrading
          )
          .div(hundredPercent),
      ]
    );
  });

  describe("Emit", function () {
    it("Emit PurchasedTradeSell(seller: address[1], sender: address[2], resoultACDMToken: 25 000, ACDMTokenPrice)", async () => {
      const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
      const value = tradeSell.price.mul(tradeSell.amount).div(2);

      await expect(
        ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[1].address, {
          value: value,
        })
      )
        .to.emit(ACDMPlatform, "PurchasedTradeSell")
        .withArgs(
          accounts[1].address,
          accounts[2].address,
          tradeSell.amount.div(2),
          tradeSell.price
        );
    });
  });

  describe("Reverted", function () {
    it("ACDMPlatform:no active sale", async () => {
      await ACDMPlatform.connect(accounts[3]).signUpWithoutReferrer();
      let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale

      const value = ethers.utils.parseEther("1.0");

      await expect(
        ACDMPlatform.connect(accounts[2]).buyTradeSell(accounts[3].address, {
          value: value,
        })
      ).to.be.revertedWith("ACDMPlatform:no active sale");
    });
  });
});
