const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

const purchasedOnSale: BigNumber = BigNumber.from(50000000000);

describe("trade init ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);

    ACDMToken.connect(accounts[1]).approve(
      ACDMPlatform.address,
      ethers.constants.MaxUint256
    );

    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.5"),
    });

    const timeStamp = await getTimestamp();
    await ethers.provider.send("evm_mine", [
      timeStamp + ((await ACDMPlatform.roundTimeSize()).toNumber() + 1),
    ]);
  });

  it("Should transfer tokens on initSell", async function () {
    await ACDMPlatform.changeState();

    const ACDMPlatformBalanceTokenBefore = await ACDMToken.balanceOf(
      ACDMPlatform.address
    );

    await ACDMPlatform.connect(accounts[1]).initSell(
      purchasedOnSale,
      await ACDMPlatform.ACDMTokenPrice()
    );

    const ACDMPlatformBalanceTokenAfter = await ACDMToken.balanceOf(
      ACDMPlatform.address
    );

    expect(
      (await ACDMPlatformBalanceTokenAfter).sub(ACDMPlatformBalanceTokenBefore)
    ).to.eq(purchasedOnSale);
  });

  it("Should init correct TradeSale", async function () {
    await ACDMPlatform.changeState();

    let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale

    await ACDMPlatform.connect(accounts[1]).initSell(
      purchasedOnSale,
      tokenPrice
    );

    const tradeSell = await ACDMPlatform.getTradeSale(accounts[1].address);
    const amount = tradeSell[0];
    const price = tradeSell[1];

    expect(amount).to.eq(purchasedOnSale);
    expect(price).to.eq(tokenPrice);
  });

  describe("Emit", function () {
    it("Emit InitTradeSell(address[1], tokenAmount: 50 000 ACDM, ACDMTokenPrice)", async () => {
      await ACDMPlatform.changeState();

      let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale

      await expect(
        ACDMPlatform.connect(accounts[1]).initSell(purchasedOnSale, tokenPrice)
      )
        .to.emit(ACDMPlatform, "InitTradeSell")
        .withArgs(accounts[1].address, purchasedOnSale, tokenPrice);
    });
  });

  describe("Reverted", function () {
    it("ACDMPlatform: trade is over", async () => {
      let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale

      await expect(
        ACDMPlatform.connect(accounts[1]).initSell(purchasedOnSale, tokenPrice)
      ).to.be.revertedWith("ACDMPlatform: trade is over");
    });

    it("ACDMPlatform: already have sale", async () => {
      await ACDMPlatform.changeState();

      let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale
      await ACDMPlatform.connect(accounts[1]).initSell(
        purchasedOnSale.div(2),
        tokenPrice
      );
      await expect(
        ACDMPlatform.connect(accounts[1]).initSell(
          purchasedOnSale.div(2),
          tokenPrice
        )
      ).to.be.revertedWith("ACDMPlatform: already have sale");
    });

    it("ACDMPlatform: less than minimum", async () => {
      await ACDMPlatform.changeState();

      let tokenPrice = await ACDMPlatform.ACDMTokenPrice(); // just take price from sale
      let tokenMinimumPurchaseSaleRound =
        await ACDMPlatform.minimumPurchaseSaleRound();

      await expect(
        ACDMPlatform.connect(accounts[1]).initSell(
          tokenMinimumPurchaseSaleRound.sub(1),
          tokenPrice
        )
      ).to.be.revertedWith("ACDMPlatform: less than minimum");
    });
  });
});
