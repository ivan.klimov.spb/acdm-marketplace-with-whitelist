const { expect, assert } = require("chai");
import { ethers } from "hardhat";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/signers";
import {
  ACDMPlatform,
  DAO,
  ERC20Token,
  IERC20,
  Staking,
} from "../../src/types";
import { chairPersonId, deployDAO } from "../DAO/deploymentDAO";
import { deploymentXXXToken } from "../XXXToken/deployXXXToken";
import { deploymentACDMToken } from "../ACDMToken/deployACDMToken";
import {
  deploymentACDMPlatform,
  PlatformState,
  startPrice,
  uniswapRouterAddress,
} from "./deploymentACDMPlatform";
import { getTimestamp } from "../getTimestamp";
import { BigNumber } from "ethers";

describe("send fee ACDMPlatform", function () {
  let dao: DAO;
  let accounts: SignerWithAddress[];
  let chairPerson: SignerWithAddress;
  let XXXToken: ERC20Token;
  let lpToken: IERC20;
  let calldata: string;
  let recipient: string;
  let stakingContract: Staking;
  let ACDMToken: ERC20Token;
  let ACDMPlatform: ACDMPlatform;

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    chairPerson = accounts[chairPersonId];

    XXXToken = await deploymentXXXToken();
    ACDMToken = await deploymentACDMToken();

    dao = await deployDAO();

    ACDMPlatform = await deploymentACDMPlatform(XXXToken, ACDMToken, dao);

    ACDMToken.grantRole(await ACDMToken.MINTER_ROLE(), ACDMPlatform.address);
  });

  it("Should without referrer save fee on platform", async function () {
    await ACDMPlatform.connect(accounts[1]).signUpWithoutReferrer();
    await ACDMPlatform.changeState();

    await ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
      value: ethers.utils.parseEther("0.1"),
    });

    expect(await ACDMPlatform.ACDMPlatformFeeAmount()).to.eq(
      BigNumber.from(ethers.utils.parseEther("0.008"))
    );
  });

  it("Should send 1 level referrer 5% and safe 3% fee for platform", async function () {
    await ACDMPlatform.connect(accounts[2]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[1]).signUp(accounts[2].address);
    await ACDMPlatform.changeState();

    await expect(() =>
      ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
        value: ethers.utils.parseEther("0.1"),
      })
    ).to.changeEtherBalances([accounts[2]], [ethers.utils.parseEther("0.005")]);

    expect(await ACDMPlatform.ACDMPlatformFeeAmount()).to.eq(
      BigNumber.from(ethers.utils.parseEther("0.003"))
    );
  });

  it("Should send 1 level referrer 5% and send 3% fee to 2 level referrer", async function () {
    await ACDMPlatform.connect(accounts[3]).signUpWithoutReferrer();
    await ACDMPlatform.connect(accounts[2]).signUp(accounts[3].address);
    await ACDMPlatform.connect(accounts[1]).signUp(accounts[2].address);
    await ACDMPlatform.changeState();

    await expect(() =>
      ACDMPlatform.connect(accounts[1]).buyTokensOnSale({
        value: ethers.utils.parseEther("0.1"),
      })
    ).to.changeEtherBalances(
      [accounts[2], accounts[3]],
      [ethers.utils.parseEther("0.005"), ethers.utils.parseEther("0.003")]
    );

    expect(await ACDMPlatform.ACDMPlatformFeeAmount()).to.eq(
      BigNumber.from(ethers.utils.parseEther("0"))
    );
  });
});
