export const TASK_MINT = "mint";
export const TASK_APPROVE = "approve";
export const TASK_ADD_LIQUIDITY = "addLiquidityETH";
export const TASK_GET_LIQUIDITY_WETH_PAIR = "getLiquidityWETHPair";
export const TASK_GIVE_MINT_PERMISSION = "addMintPermission";

export const TASK_STAKE = "stake";
export const TASK_CLAIM = "claim";
export const TASK_UNSTAKE = "unstake";

export const TASK_ADD_PROPOSAL = "addproposal";
export const TASK_VOTE = "vote";
export const TASK_FINISH = "finishvoting";

export const TASK_BUY_TOKENS_ON_SALE = "buyTokensOnSale";
export const TASK_BUY_TRADE_SELL = "buyTradeSell";
export const TASK_INIT_SELL = "initSell";
export const TASK_STOP_SELL = "stopSell";
export const TASK_CHANGE_PLATFORM_STATE = "changeState";

export const TASK_SIGN_UP_WITHOUT_REFERRER = "signUpWithoutReferrer";
export const TASK_SIGN_UP = "signUp";
export const TASK_SET_REFERRER = "setReferrer";
