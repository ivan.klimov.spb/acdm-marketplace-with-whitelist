import { TASK_ADD_LIQUIDITY } from "./task-names";
import { task } from "hardhat/config";

task(TASK_ADD_LIQUIDITY, "Add liquidity to uniswap")
  .addParam("signer", "Account signing the transaction")
  .addParam("token", "XXXToken address")
  .addParam("value", "amount of XXXToken")
  .addParam("eth", "amount of eth")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let uniswapV2Router01 = await hre.ethers.getContractAt(
      "IUniswapV2Router01",
      process.env.ROUTER_ADDRESS as string
    );

    const blockNumber = await hre.ethers.provider.getBlockNumber();
    const block = await hre.ethers.provider.getBlock(blockNumber);
    const timeStamp = block.timestamp + 30;

    await uniswapV2Router01
      .connect(account)
      .addLiquidityETH(
        args.token,
        args.value,
        args.value,
        args.eth,
        account.address,
        timeStamp,
        {
          value: args.eth, //eth value
        }
      );

    console.log("task create liquidity pair to uniswap finished");
  });
