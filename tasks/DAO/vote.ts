import { task } from "hardhat/config";
import { TASK_VOTE } from "../task-names";

task(TASK_VOTE, "Vote")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("id", "proposal Id")
  .addParam("agree", "is agree")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let dao = await hre.ethers.getContractAt("DAO", args.contract);

    await dao.connect(account).vote(args.id, args.agree);

    console.log("task Vote finished");
  });
