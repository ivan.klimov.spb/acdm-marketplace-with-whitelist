import { task } from "hardhat/config";
import { TASK_ADD_PROPOSAL } from "../task-names";

task(TASK_ADD_PROPOSAL, "Add proposal")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("callData", "bytes memory callData")
  .addParam("recipient", "recipient address")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let dao = await hre.ethers.getContractAt("DAO", args.contract);

    await dao.connect(account).addProposal(args.callData, args.recipient);

    console.log("task Add proposal finished");
  });
