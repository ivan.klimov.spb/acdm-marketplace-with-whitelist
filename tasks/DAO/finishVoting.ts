import { task } from "hardhat/config";
import { TASK_FINISH } from "../task-names";

task(TASK_FINISH, "Finish proposal")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("id", "proposal Id")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let dao = await hre.ethers.getContractAt("DAO", args.contract);

    await dao.connect(account).finishProposal(args.id);

    console.log("task Finish proposal finished");
  });
