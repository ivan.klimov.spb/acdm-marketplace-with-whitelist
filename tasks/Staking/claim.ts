import { task } from "hardhat/config";
import { TASK_CLAIM } from "../task-names";

task(TASK_CLAIM, "Claim reward")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let staking = await hre.ethers.getContractAt("Staking", args.contract);

    await staking.connect(account).claim();

    console.log("task claim finished");
  });
