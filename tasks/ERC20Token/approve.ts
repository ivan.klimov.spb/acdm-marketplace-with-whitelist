import { task } from "hardhat/config";
import { TASK_APPROVE } from "../task-names";

task(TASK_APPROVE, "Give approve")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("spender", "address account that can spend tokens")
  .addParam("value", "token value")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let erc20Token = await hre.ethers.getContractAt("IERC20", args.contract);

    await erc20Token.connect(account).approve(args.spender, args.value);

    console.log("task approve finished");
  });
