import { task } from "hardhat/config";
import { TASK_SET_REFERRER } from "../task-names";

task(TASK_SET_REFERRER, "set Referrer ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("referrer", "referrer address")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).setReferrer(args.referrer);

    console.log("task set Referrer finished");
  });
