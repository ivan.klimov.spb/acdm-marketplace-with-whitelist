import { task } from "hardhat/config";
import { TASK_BUY_TRADE_SELL } from "../task-names";

task(TASK_BUY_TRADE_SELL, "buy Trade Sell ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .addParam("seller", "seller address")
  .addParam("eth", "amount of eth")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).buyTradeSell(args.seller, {
      value: args.eth, //eth value
    });

    console.log("task buy Trade Sell finished");
  });
