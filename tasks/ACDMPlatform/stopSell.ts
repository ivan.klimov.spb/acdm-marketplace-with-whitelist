import { task } from "hardhat/config";
import { TASK_STOP_SELL } from "../task-names";

task(TASK_STOP_SELL, "stop Sell ACDMPlatform")
  .addParam("contract", "contract address")
  .addParam("signer", "Account signing the transaction")
  .setAction(async (args, hre) => {
    let account = await hre.ethers.getSigner(args.signer);

    let ACDMPlatform = await hre.ethers.getContractAt(
      "ACDMPlatform",
      args.contract
    );

    await ACDMPlatform.connect(account).stopSell();

    console.log("task stopSell finished");
  });
