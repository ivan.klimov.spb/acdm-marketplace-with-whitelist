import "./task-names";
import "./ERC20Token/mint";
import "./ERC20Token/approve";
import "./addLiquidityETH";
import "./getLiquidityWETHPair";
import "./ERC20Token/addMintPermission";

//Staking
import "./Staking/stake";
import "./Staking/claim";
import "./Staking/unstake";

//DAO
import "./DAO/addProposal";
import "./DAO/vote";
import "./DAO/finishVoting";

//ACDMPlatform
import "./ACDMPlatform/buyTokensOnSale";
import "./ACDMPlatform/buyTradeSell";
import "./ACDMPlatform/initSell";
import "./ACDMPlatform/stopSell";
import "./ACDMPlatform/changeState";

import "./ACDMPlatform/signUpWithoutReferrer";
import "./ACDMPlatform/signUp";
import "./ACDMPlatform/setReferrer";
