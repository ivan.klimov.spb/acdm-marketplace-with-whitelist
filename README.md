# Do this first

Add your file .env to root project

Structure .env

```
PRIVATE_KEY = string
PUBLIC_KEY = string
PRIVATE_KEY2 = string
PUBLIC_KEY2 = string
PRIVATE_KEY3 = string
PUBLIC_KEY3 = string
API_URL_RINKEBY = string
API_URL_GOERLI = string
API_KEY_ETHERSCAN = string
API_KEY_BSCSCAN = string
FACTORY_ADDRESS = "0x5C69bEe701ef814a2B6a3EDD4B1652CB9cc5aA6f"
ROUTER_ADDRESS = "0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D"
LP_TOKEN_ADDRESS = string
XXX_TOKEN_ADDRESS = string
ACDM_TOKEN_ADDRESS = string
DAO_ADDRESS = string
STAKING_ADDRESS = string
ACDM_PLATFORM_ADDRESS = string
```

# Start

```
npm install
npx hardhat compile
```

# Run test

```
npx hardhat coverage
```

# Deploy contract to rinkeby 

## ERC20 XXXToken
```
npx hardhat run ./scripts/XXXTokenDeploy.ts --network rinkeby
```
mint XXXToken
approve for uniswap router 
addLiquidityETH

## ERC20 ACDMToken
```
npx hardhat run ./scripts/ACDMTokenDeploy.ts --network rinkeby
```

## Staking
```
npx hardhat run ./scripts/StakingDeploy.ts --network rinkeby
```

## DAO
```
npx hardhat run ./scripts/DAODeploy.ts --network rinkeby
```

setStakingAddress for DAO
setDAO for Staking

## ACDMPlatform
```
npx hardhat run ./scripts/ACDMPlatformDeploy.ts --network rinkeby
```
addMintPermission ACDMToken for ACDMPlatform

# Look at tasks list

```
npx hardhat  --help
```
